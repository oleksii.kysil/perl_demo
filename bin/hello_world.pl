#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;

print "Hello World!!!!!!\n";

my $product_name = q{EasyCruit};
printf "%s world domination!!!!\n", $product_name;

$product_name = qq{EasyCruit\n};
print 'This is ' . $product_name;

my $line_with_sprintf = sprintf 'This is %s', $product_name;
print $line_with_sprintf;

print q{This is single quotes btw\n};

my $multiline_value = << 'EOF';
This is
a multiline
string value
EOF

print $multiline_value;
exit 0;
