#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;

BEGIN {
    use Cwd qw(abs_path);
    use File::Basename;
    use File::Spec;

    my $base_path = dirname( dirname( abs_path($0) ) );
    my $lib_path = File::Spec->catdir( $base_path, 'lib' );
    unshift @INC, $lib_path;
}

use Demo::Class::Modern::BaseClass;
use Demo::Class::Modern::Person;

my $person = Demo::Class::Modern::Person->new(
    first_name        => 'Fredrik',
    last_name         => 'Fungusen',
    greeting_template => "This is classic greeting for %s\n",
    age               => 10,
);
print $person->create_greeting();

printf "Person's age is %d\n", $person->age();

exit 0;
