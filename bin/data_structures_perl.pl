#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;

use Data::Dumper;

my $scalar_value = 100500;
print Dumper($scalar_value);

$scalar_value = 'This is also scalar';

# Please never use this
unless ( ref $scalar_value ) {
    print "Is not a reference yet\n";
}
print Dumper($scalar_value);

my @array_value = split q{}, $scalar_value;
my %hash_value = map { $_ => length($_) } split( q{ }, $scalar_value );
print Dumper(@array_value);

printf "Value at position 5 is %s\n", $array_value[5];

$scalar_value = \@array_value;

if ( ref $scalar_value ) {
    print "Already a reference\n";
}
print Dumper($scalar_value);
printf "Value at position 5 is %s\n", $scalar_value->[5];

print Dumper(%hash_value);

printf qq{Number of letter in the word "also" is : %s\n}, $hash_value{also};

$scalar_value = \%hash_value;
print Dumper($scalar_value);

printf qq{Number of letter in the word "also" is : %s\n}, $scalar_value->{also};

my $first_letter = shift @array_value;
printf "First letter was %s\n", $first_letter;
push @array_value,, $first_letter;
print Dumper(\@array_value);
$first_letter = pop @array_value;
unshift @array_value, $first_letter;
print Dumper(\@array_value);

# yes we can do it in perl
@array_value = %hash_value;
print Dumper( \@array_value );

exit 0;