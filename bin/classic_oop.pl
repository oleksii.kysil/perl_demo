#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;

BEGIN {
    use Cwd qw(abs_path);
    use File::Basename;
    use File::Spec;

	my $base_path = dirname( dirname( abs_path($0) ) );
    my $lib_path = File::Spec->catdir( $base_path, 'lib' );
    unshift @INC, $lib_path;
}

use Demo::Class::Classic::PersonGreeting;

my $person_greeting = Demo::Class::Classic::PersonGreeting->new(
    first_name       => 'Fredrik',
    last_name        => 'Fungusen',
    greeting_pattern => "This is classic greeting for %s\n",
);
print $person_greeting->create_greeting();

exit 0;
