#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;

BEGIN {
    use Cwd qw(abs_path);
    use File::Basename;
    use File::Spec;

	my $base_path = dirname( dirname( abs_path($0) ) );
    my $lib_path = File::Spec->catdir( $base_path, 'lib' );
    unshift @INC, $lib_path;
}

use Demo::Module::Greeting qw(create_greeting_with_signature);

my $first_name = 'Fungusen';
my $last_name = 'Camper';
print create_greeting_with_signature( $first_name, $last_name );
print Demo::Module::Greeting::create_greeting( $first_name, $last_name );
print Demo::Module::Greeting::create_greeting_pass_params( $first_name, $last_name );
print Demo::Module::Greeting::create_greeting_weird_params( $first_name, $last_name );


exit 0;
