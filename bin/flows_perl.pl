#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;

use Data::Dumper;

my @array_value = ( 10, 20, 30, 40, 50 );
my %hash_value = ( key1 => 100, key2 => 200, key3 => 300 );

my $i = 0;
while ( $i < scalar(@array_value) ) {
    printf "Value is %d\n", $array_value[$i];
    $i++;
}

for ( my $j = 0 ; $j < scalar(@array_value) ; $j++ ) {
    printf "Value using for is %d\n", $array_value[$j];
}

foreach (@array_value) {
    printf "Value using foreach is %d\n", $_;
}

foreach my $value (@array_value) {
    printf "Value using correct foreach is %d\n", $value;
}

foreach my $key ( keys(%hash_value) ) {
    printf "Value from hash using foreach is %d\n", $hash_value{$key};
}

foreach my $key ( sort keys(%hash_value) ) {
    printf "Value from hash using consistent foreach is %d\n",
      $hash_value{$key};
}

my $array_ref = \@array_value;
my $hash_ref  = \%hash_value;

foreach my $value ( @{$array_ref} ) {
    if ( $value > 20 ) {
        last;
    }
    printf "Value using array reference is %d\n", $value;
}

foreach my $key ( keys( %{$hash_ref} ) ) {
    my $value = $hash_value{$key};
    unless ( $value > 250 ) {
        printf "Value from hash reference is small %d\n", $value;
        next;
    }
    printf "Value from hash reference is %d\n", $value;
}

my $top_values_array = [ grep { $_ > 25 } @array_value ];
my $top_values_hash = { map { $_ => $hash_value{$_} }
      grep { $hash_value{$_} > 250 } keys(%hash_value) };

print Dumper($top_values_array);
print Dumper($top_values_hash);

exit 0;
