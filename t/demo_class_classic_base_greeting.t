#!/usr/bin/perl

use strict;
use warnings;
use utf8;

BEGIN {
    use Cwd qw(abs_path);
    use File::Basename;
    use File::Spec;

    my $libpath =
      File::Spec->catdir( dirname( dirname( abs_path($0) ) ), 't', 'tlib' );
    unshift @INC, $libpath;
    $libpath =
      File::Spec->catdir( dirname( dirname( abs_path($0) ) ), 'lib' );
    unshift @INC, $libpath;
}

use Demo::Class::Classic::BaseGreetingTest;

Demo::Class::Classic::BaseGreetingTest->new()->runtests();
exit 0;