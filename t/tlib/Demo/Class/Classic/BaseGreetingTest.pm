package Demo::Class::Classic::BaseGreetingTest;

use strict;
use warnings FATAL => 'all';
use utf8;

use parent qw(Test::Class);

use Test::Deep qw(cmp_deeply);
use Test::Exception;
use Test::MockObject::Extends;
use Demo::Class::Classic::BaseGreeting;

sub setup : Test(setup) {
    my $self = shift;

    $self->{_instance} =
      Test::MockObject::Extends->new('Demo::Class::Classic::BaseGreeting');
    return;
}

sub test_create_greeting : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    $instance->set_always( 'get_full_name', 'Test Full Name' );
    $instance->{_greeting_pattern} = 'Test: %s';
    my $result = $instance->create_greeting();
    cmp_deeply( $result, 'Test: Test Full Name', 'Greeting is generated' );
    return;
}

sub test_get_full_name : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    dies_ok(
        sub { $instance->get_full_name() },
        'Method get_full_name is abstract'
    );
    return;
}

1;
