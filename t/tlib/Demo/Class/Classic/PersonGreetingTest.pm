package Demo::Class::Classic::PersonGreetingTest;

use strict;
use warnings FATAL => 'all';
use utf8;

use parent qw(Test::Class);

use Test::Deep qw(cmp_deeply);
use Demo::Class::Classic::PersonGreeting;

sub setup : Test(setup) {
    my $self = shift;

    $self->{_instance} = Demo::Class::Classic::PersonGreeting->new(
        first_name       => 'Test',
        last_name        => 'User',
        greeting_pattern => 'This is a test greeting for %s',
    );
    return;
}

sub test_create_greeting : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->create_greeting();
    cmp_deeply(
        $result,
        'This is a test greeting for Test User',
        'Greeting for person is generated'
    );
    return;
}

sub test_get_full_name : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->get_full_name();
    cmp_deeply( $result, 'Test User', 'Full name for person is generated' );
    return;
}

1;
