package Demo::Class::Modern::BaseClassTest;

use strict;
use warnings FATAL => 'all';
use utf8;

use parent qw(Test::Class);

use Readonly;
use Test::Deep qw(cmp_deeply);
use Demo::Class::Modern::BaseClass;

Readonly my $TEST_AGE => 100500;

sub setup : Test(setup) {
    my $self = shift;

    $self->{_instance} =
      Demo::Class::Modern::BaseClass->new( age => $TEST_AGE );
    return;
}

sub test_age : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->age();
    cmp_deeply( $result, $TEST_AGE, 'Age is set' );
    return;
}

1;
