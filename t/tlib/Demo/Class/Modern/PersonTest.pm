package Demo::Class::Modern::PersonTest;

use strict;
use warnings FATAL => 'all';
use utf8;

use parent qw(Test::Class);

use Readonly;
use Test::Deep qw(cmp_deeply);
use Demo::Class::Modern::Person;

Readonly my $TEST_AGE        => 50;
Readonly my $TEST_FIRST_NAME => 'Test name';
Readonly my $TEST_LAST_NAME  => 'Test surname';

sub setup : Test(setup) {
    my $self = shift;

    $self->{_instance} = Demo::Class::Modern::Person->new(
        first_name        => $TEST_FIRST_NAME,
        last_name         => $TEST_LAST_NAME,
        greeting_template => 'This is test greeting for %s',
        age               => $TEST_AGE,
    );
    return;
}

sub test_age : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->age();
    cmp_deeply( $result, $TEST_AGE, 'Age is set' );
    return;
}

sub test_first_name : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->first_name();
    cmp_deeply( $result, $TEST_FIRST_NAME, 'First name is set' );
    return;
}

sub test_last_name : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->last_name();
    cmp_deeply( $result, $TEST_LAST_NAME, 'Last name is set' );
    return;
}

sub test_get_full_name : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->get_full_name();
    my $expected = sprintf '%s %s', $TEST_FIRST_NAME, $TEST_LAST_NAME;
    cmp_deeply( $result, $expected, 'Full name is generated' );
    return;
}

sub test_create_greeting : Test(1) {
    my $self = shift;

    my $instance = $self->{_instance};
    my $result   = $instance->create_greeting();
    my $expected = sprintf 'This is test greeting for %s %s', $TEST_FIRST_NAME,
      $TEST_LAST_NAME;
    cmp_deeply( $result, $expected, 'Greeting is generated' );
    return;
}

1;
