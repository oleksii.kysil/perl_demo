package Demo::Module::GreetingTest;

use strict;
use warnings FATAL => 'all';
use utf8;

use parent qw(Test::Class);

use Test::Deep qw(cmp_deeply);
use Test::Exception;
use Demo::Module::Greeting qw(create_greeting_with_signature);

sub test_create_greeting_with_signature : Test(5) {
    my $self = shift;

    throws_ok(
        sub { create_greeting_with_signature( undef, undef ) },
        qr/first name/i,
        'First name is undefined'
    );
    throws_ok(
        sub { create_greeting_with_signature( q{}, undef ) },
        qr/first name/i,
        'First name is empty'
    );
    throws_ok( sub { create_greeting_with_signature( 'First name', undef ) },
        qr/last name/i, 'Last name is undefined' );
    throws_ok( sub { create_greeting_with_signature( 'First name', q{} ) },
        qr/last name/i, 'Last name is empty' );
    my $result = create_greeting_with_signature( 'First', 'Last' );
    cmp_deeply(
        $result,
        "Hello! Welcome to Perl World: First Last!!!!\n",
        'Greeting is correct'
    );
    return;
}

sub test_create_greeting : Test(5) {
    my $self = shift;

    throws_ok(
        sub { Demo::Module::Greeting::create_greeting( undef, undef ) },
        qr/first name/i,
        'First name is undefined'
    );
    throws_ok(
        sub { Demo::Module::Greeting::create_greeting( q{}, undef ) },
        qr/first name/i,
        'First name is empty'
    );
    throws_ok(
        sub { Demo::Module::Greeting::create_greeting( 'First name', undef ) },
        qr/last name/i,
        'Last name is undefined'
    );
    throws_ok(
        sub { Demo::Module::Greeting::create_greeting( 'First name', q{} ) },
        qr/last name/i, 'Last name is empty' );
    my $result = Demo::Module::Greeting::create_greeting( 'First', 'Last' );
    cmp_deeply(
        $result,
        "Hello! Welcome to Perl World: First Last!!!!\n",
        'Greeting is correct'
    );
    return;
}

sub test_create_greeting_weird_params : Test(5) {
    my $self = shift;

    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_weird_params( undef, undef );
        },
        qr/first name/i,
        'First name is undefined'
    );
    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_weird_params( q{}, undef );
        },
        qr/first name/i,
        'First name is empty'
    );
    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_weird_params( 'First name',
                undef );
        },
        qr/last name/i,
        'Last name is undefined'
    );
    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_weird_params( 'First name',
                q{} );
        },
        qr/last name/i,
        'Last name is empty'
    );
    my $result =
      Demo::Module::Greeting::create_greeting_weird_params( 'First', 'Last' );
    cmp_deeply(
        $result,
        "Hello! Welcome to Perl World: First Last!!!!\n",
        'Greeting is correct'
    );
    return;
}

sub test_create_greeting_pass_params : Test(5) {
    my $self = shift;

    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_pass_params( undef, undef );
        },
        qr/first name/i,
        'First name is undefined'
    );
    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_pass_params( q{}, undef );
        },
        qr/first name/i,
        'First name is empty'
    );
    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_pass_params( 'First name',
                undef );
        },
        qr/last name/i,
        'Last name is undefined'
    );
    throws_ok(
        sub {
            Demo::Module::Greeting::create_greeting_pass_params( 'First name',
                q{} );
        },
        qr/last name/i,
        'Last name is empty'
    );
    my $result =
      Demo::Module::Greeting::create_greeting_pass_params( 'First', 'Last' );
    cmp_deeply(
        $result,
        "Hello! Welcome to Perl World: First Last!!!!\n",
        'Greeting is correct'
    );
    return;
}

1;
