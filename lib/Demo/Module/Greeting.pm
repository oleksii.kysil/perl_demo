package Demo::Module::Greeting;

use strict;
use warnings FATAL => 'all';
use experimental 'signatures';
use feature 'signatures';
use utf8;

use Carp qw(croak);
use Readonly;
use Exporter 'import';
our @EXPORT_OK = qw(create_greeting_with_signature);

Readonly my $GREETING_TEMPALTE => "Hello! Welcome to Perl World: %s %s!!!!\n";

sub create_greeting_with_signature ( $first_name, $last_name ) {
    if ( !defined $first_name || $first_name eq q{} ) {
        croak('First name is invalid');
    }

    if ( !defined $last_name || $last_name eq q{} ) {
        croak('Last name is invalid');
    }

    return sprintf( $GREETING_TEMPALTE, $first_name, $last_name );
}

sub create_greeting {
    my ( $first_name, $last_name ) = @_;

    return create_greeting_with_signature( $first_name, $last_name );
}

sub create_greeting_weird_params {
    return create_greeting_with_signature( $_[0], $_[1] );
}

sub create_greeting_pass_params {
    return create_greeting_with_signature(@_);
}

1;
