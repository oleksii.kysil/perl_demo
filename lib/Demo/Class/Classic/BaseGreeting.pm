package Demo::Class::Classic::BaseGreeting;

use strict;
use warnings FATAL => 'all';
use utf8;

use Carp;

sub new {
    my ( $class, %args ) = @_;

    $class = ref($class) || $class;
    my $self = { _greeting_pattern => $args{greeting_pattern} };
    bless $self, $class;
    return $self;
}

sub get_full_name {
    croak('Unimplemented method');
}

sub create_greeting {
    my $self = shift;

    my $result = sprintf $self->{_greeting_pattern}, $self->get_full_name();
    return $result;
}

1;
