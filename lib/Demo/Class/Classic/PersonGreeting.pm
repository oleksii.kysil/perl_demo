package Demo::Class::Classic::PersonGreeting;

use strict;
use warnings FATAL => 'all';
use utf8;

use parent qw(Demo::Class::Classic::BaseGreeting);

sub new {
    my ( $class, %args ) = @_;

    $class = ref($class) || $class;
    my $self = $class->SUPER::new(%args);
    $self->{_first_name} = $args{first_name};
    $self->{_last_name}  = $args{last_name};
    return $self;
}

sub get_full_name {
    my $self = shift;

    return join( q{ }, $self->{_first_name}, $self->{_last_name} );
}

1;
