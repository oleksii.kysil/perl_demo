package Demo::Class::Modern::Person;
use utf8;

use Moose;
use namespace::autoclean;

extends 'Demo::Class::Modern::BaseClass';

has '+age' => (
    is       => 'rw',
    isa      => 'Int',
    required => 1,
);

has 'first_name' => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has 'last_name' => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has 'greeting_template' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
    reader   => 'get_greeting_template',
);

with 'Demo::Class::Modern::GreetingRole';

sub get_full_name {
    my $self = shift;

    return join( q{ }, $self->first_name(), $self->last_name() );
}

__PACKAGE__->meta->make_immutable();
no utf8;

1;
