package Demo::Class::Modern::GreetingRole;

use utf8;

use Moose::Role;
use namespace::autoclean;

requires qw(get_full_name get_greeting_template);

sub create_greeting {
    my $self = shift;

    my $result = sprintf $self->get_greeting_template, $self->get_full_name();
    return $result;
}

no utf8;

1;
