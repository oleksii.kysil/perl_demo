package Demo::Class::Modern::BaseClass;

use utf8;

use Moose;
use namespace::autoclean;

has age => (
    is      => 'rw',
    isa     => 'Int',
    default => 0,
);

__PACKAGE__->meta->make_immutable();
no utf8;

1;
