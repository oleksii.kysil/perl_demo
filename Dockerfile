FROM perl:latest

RUN apt-get update && apt-get -y install libmoose-perl libreadonly-perl libtest-harness-perl libjson-xs-perl \
 libtest-class-perl libtest-deep-perl libtest-mockobject-perl libtest-exception-perl libnamespace-autoclean-perl

RUN mkdir -p /usr/src/app
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

ENTRYPOINT ["tail"]